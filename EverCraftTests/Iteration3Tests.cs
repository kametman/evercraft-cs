﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EverCraft;

namespace EverCraftTests
{
    [TestClass]
    public class Iteration3Tests
    {
        private ECCharacter character;
        private ECCharacter opponent;

        [TestInitialize]
        public void Init()
        {
            character = new ECCharacter();
            opponent = new ECCharacter();
        }

        //  HUMAN
        [TestMethod]
        public void EverCraft_Characters_Default_To_Human()
        {
            Assert.AreEqual(Race.Human, character.Race);
        }

        //  ORC
        [TestMethod]
        public void EverCraft_Orc_Gains_Strength_Bonus()
        {
            character.Race = Race.Orc;

            Assert.AreEqual(2, character.StrengthModifier);
        }
        [TestMethod]
        public void EverCraft_Orc_Gains_Intelligence_Penalty()
        {
            character.Race = Race.Orc;

            Assert.AreEqual(-1, character.IntelligenceModifier);
        }
        [TestMethod]
        public void EverCraft_Orc_Gains_Wisdom_Penalty()
        {
            character.Race = Race.Orc;

            Assert.AreEqual(-1, character.WisdomModifier);
        }
        [TestMethod]
        public void EverCraft_Orc_Gains_Charisma_Penalty()
        {
            character.Race = Race.Orc;

            Assert.AreEqual(-1, character.CharismaModifier);
        }
        [TestMethod]
        public void EverCraft_Orc_Gains_Armor_Class_Bonus()
        {
            character.Race = Race.Orc;

            Assert.AreEqual(12, character.ArmorClass);
        }

        //  DWARF
        [TestMethod]
        public void EverCraft_Dwarf_Gains_Constitution_Bonus()
        {
            character.Race = Race.Dwarf;

            Assert.AreEqual(1, character.ConstitutionModifier);
        }
        [TestMethod]
        public void EverCraft_Dwarf_Gains_Charisma_Penalty()
        {
            character.Race = Race.Dwarf;

            Assert.AreEqual(-1, character.CharismaModifier);
        }
        [TestMethod]
        public void EverCraft_Dwarf_Gains_Double_Constitution_Bonus_To_Hit_Points()
        {
            character.Race = Race.Dwarf;

            Assert.AreEqual(7, character.HitPoints);
        }
        [TestMethod]
        public void EverCraft_Dwarf_Gains_Bonus_To_Attack_Roll_Against_Orcs()
        {
            character.Race = Race.Dwarf;
            opponent.Race = Race.Orc;
            bool success = character.Attack(opponent, 10);

            Assert.AreEqual(true, success);
        }
        [TestMethod]
        public void EverCraft_Dwarf_Gains_Bonus_To_Damage_Roll_Against_Orcs()
        {
            character.Race = Race.Dwarf;
            opponent.Race = Race.Orc;
            character.Attack(opponent, 10);

            Assert.AreEqual(2, opponent.HitPoints);
        }

        //  ELF
        [TestMethod]
        public void EverCraft_Elf_Gains_Dexterity_Bonus()
        {
            character.Race = Race.Elf;

            Assert.AreEqual(1, character.DexterityModifier);
        }
        [TestMethod]
        public void EverCraft_Elf_Gains_Constitution_Penalty()
        {
            character.Race = Race.Elf;

            Assert.AreEqual(-1, character.ConstitutionModifier);
        }
        [TestMethod]
        public void EverCraft_Elf_Has_Expanded_Critical_Hit_Range()
        {
            character.Race = Race.Elf;
            character.Attack(opponent, 19);

            Assert.AreEqual(3, opponent.HitPoints);

        }
        [TestMethod]
        public void EverCraft_Elf_Has_Bonus_To_ArmorClass_Against_Orcs()
        {
            character.Race = Race.Elf;
            opponent.Race = Race.Orc;
            bool success = opponent.Attack(character, 10);

            Assert.AreEqual(false, success);
        }

        //  HALFLING
        [TestMethod]
        public void EverCraft_Halfling_Gains_Deterity_Bonus()
        {
            character.Race = Race.Halfling;

            Assert.AreEqual(1, character.DexterityModifier);

        }
        [TestMethod]
        public void EverCraft_Halfling_Gains_Strength_Penalty()
        {
            character.Race = Race.Halfling;

            Assert.AreEqual(-1, character.StrengthModifier);
        }
        [TestMethod]
        public void EverCraft_Halfling_Has_Has_Bonus_To_Armor_Class_Against_Non_Halflings()
        {
            character.Race = Race.Halfling;
            bool success = opponent.Attack(character, 10);

            Assert.AreEqual(false, success);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EverCraft_Halfling_Cannont_Be_Evil_Alignment()
        {
            character.Race = Race.Halfling;
            character.Alignment = Alignment.Evil;
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EverCraft_Halfling_Cannont_Be_Evil_Alignment2()
        {
            character.Alignment = Alignment.Evil;
            character.Race = Race.Halfling;
        }
    }
}
