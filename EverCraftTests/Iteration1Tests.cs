﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EverCraft;

namespace EverCraftTests
{
    [TestClass]
    public class Iteration1Tests
    {
        private ECCharacter character;
        private ECCharacter opponent;

        [TestInitialize]
        public void Init()
        {
            character = new ECCharacter();
            opponent = new ECCharacter();
        }

        [TestMethod]
        public void EverCraft_Character_Has_Name()
        {
            character.Name = "Eva Kraft";

            Assert.AreEqual("Eva Kraft", character.Name);
        }

        [TestMethod]
        public void EverCraft_Character_Has_Alignment()
        {
            Assert.AreEqual(Alignment.Neutral, character.Alignment);
        }

        [TestMethod]
        public void EverCraft_Character_Has_Armor_Class()
        {
            Assert.AreEqual(10, character.ArmorClass);
        }

        [TestMethod]
        public void EverCraft_Character_Has_Hit_Points()
        {
            Assert.AreEqual(5, character.HitPoints);
        }

        [TestMethod]
        public void EverCraft_Character_Can_Attack()
        {
            bool success = character.Attack(opponent, 10);

            Assert.AreEqual(true, success);
        }

        [TestMethod]
        public void EverCraft_Character_Is_Attacked_Success()
        {
            bool wasHit = character.IsHit(10, 0);

            Assert.AreEqual(true, wasHit);
        }

        [TestMethod]
        public void EverCraft_Character_Is_Attacked_Failed()
        {
            bool wasHit = character.IsHit(9, 0);

            Assert.AreEqual(false, wasHit);
        }

        [TestMethod]
        public void EverCraft_Character_Takes_Damage()
        {
            character.Attack(opponent, 10);

            Assert.AreEqual(4, opponent.HitPoints);
        }

        [TestMethod]
        public void EverCraft_Character_Takes_Critical_Hit()
        {
            character.Attack(opponent, 20);

            Assert.AreEqual(3, opponent.HitPoints);
        }

        [TestMethod]
        public void EverCraft_Character_Is_Alive()
        {
            character.TakeDamage(1);

            Assert.AreEqual(true, character.IsAlive);
        }

        [TestMethod]
        public void EverCraft_Character_Is_Dead()
        {
            character.TakeDamage(5);

            Assert.AreEqual(false, character.IsAlive);
        }

        [TestMethod]
        public void EverCraft_Character_Has_Ability_Scores()
        {
            Assert.AreEqual(10, character.Strength);
            Assert.AreEqual(10, character.Dexterity);
            Assert.AreEqual(10, character.Constitution);
            Assert.AreEqual(10, character.Intelligence);
            Assert.AreEqual(10, character.Wisdom);
            Assert.AreEqual(10, character.Charisma);
        }

        [TestMethod]
        public void EverCraft_Character_Has_Ability_Modifiers()
        {
            character.Strength = 2;
            character.Dexterity = 18;
            character.Constitution = 15;
            character.Intelligence = 20;
            character.Wisdom = 9;

            Assert.AreEqual(-4, character.StrengthModifier);
            Assert.AreEqual(4, character.DexterityModifier);
            Assert.AreEqual(2, character.ConstitutionModifier);
            Assert.AreEqual(5, character.IntelligenceModifier);
            Assert.AreEqual(-1, character.WisdomModifier);
            Assert.AreEqual(0, character.CharismaModifier);
        }

        [TestMethod]
        public void EverCraft_Character_Uses_Strength_Modifier_To_Attack_Rolls()
        {
            character.Strength = 20;
            bool success = character.Attack(opponent, 5);

            Assert.AreEqual(true, success);
        }

        [TestMethod]
        public void EverCraft_Character_Uses_Strength_Modifier_To_Damage_Rolls()
        {
            character.Strength = 14;
            character.Attack(opponent, 10);

            Assert.AreEqual(2, opponent.HitPoints);
        }

        [TestMethod]
        public void EverCraft_Character_Uses_Twice_Strength_Modifier_To_Critical_Hit()
        {
            character.Strength = 14;
            character.Attack(opponent, 20);

            Assert.AreEqual(-1, opponent.HitPoints);
        }

        [TestMethod]
        public void EverCraft_Character_Takes_Minimum_One_Damage()
        {
            character.Strength = 1;
            character.Attack(opponent, 20);

            Assert.AreEqual(4, opponent.HitPoints);
        }

        [TestMethod]
        public void EverCraft_Character_Uses_Dexterity_Modifier_To_Armor_Class()
        {
            character.Dexterity = 16;

            Assert.AreEqual(13, character.ArmorClass);
        }

        [TestMethod]
        public void EverCraft_Character_Uses_Constitution_Modifier_To_Hit_Points()
        {
            character.Constitution = 12;

            Assert.AreEqual(6, character.HitPoints);
        }

        [TestMethod]
        public void EverCraft_Character_Gains_Experience_After_Every_Successful_Attack()
        {
            character.Attack(opponent, 10);

            Assert.AreEqual(10, character.Experience);
        }

        [TestMethod]
        public void EverCraft_Character_Starts_At_Level_One()
        {
            Assert.AreEqual(1, character.Level);
        }

        [TestMethod]
        public void EverCraft_Character_Gains_A_Level_Every_One_Thousand_Experience_Points()
        {
            character.Experience = 2000;

            Assert.AreEqual(3, character.Level);
        }

        [TestMethod]
        public void EverCraft_Character_Gains_Five_Additional_Hit_Points_Per_Level()
        {
            character.Experience = 2000;

            Assert.AreEqual(15, character.HitPoints);
        }

        [TestMethod]
        public void EverCraft_Character_Gains_Attack_Bonus_For_Every_Even_Level()
        {
            character.Experience = 4000;
            bool success = character.Attack(opponent, 8);

            Assert.AreEqual(true, success);
        }
    }
}
