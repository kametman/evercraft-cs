﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EverCraft;
using EverCraft.Classes;

namespace EverCraftTests
{
    [TestClass]
    public class Iteration2Tests
    {
        private ECFighter fighter;
        private ECRogue rogue;
        private ECMonk monk;
        private ECPaladin paladin;
        private ECCharacter opponent;

        [TestInitialize]
        public void Init()
        {
            fighter = new ECFighter();
            rogue = new ECRogue();
            monk = new ECMonk();
            paladin = new ECPaladin();
            opponent = new ECCharacter();
        }

        //  FIGHTER
        [TestMethod]
        public void EverCraft_Fighter_Attack_Bonus_Increase_Every_Level()
        {
            fighter.Experience = 4000;
            bool success = fighter.Attack(opponent, 5);

            Assert.AreEqual(true, success);
        }
        [TestMethod]
        public void EverCraft_Fighter_Gains_Ten_Hit_Points_Per_Level()
        {
            Assert.AreEqual(10, fighter.HitPoints);
        }

        //  ROGUE
        [TestMethod]
        public void EverCraft_Rogue_Deals_Triple_Damage_On_Critical_Hits()
        {
            rogue.Attack(opponent, 20);

            Assert.AreEqual(2, opponent.HitPoints);
        }
        [TestMethod]
        public void EverCraft_Rogue_Ignores_Dexterity_Bonus_To_Armor_Class()
        {
            opponent.Dexterity = 20;
            bool success = rogue.Attack(opponent, 10);

            Assert.AreEqual(true, success);
        }
        [TestMethod]
        public void EverCraft_Rogue_Uses_Dexterity_Bonus_For_Attack_Rolls()
        {
            rogue.Dexterity = 20;
            bool success = rogue.Attack(opponent, 5);

            Assert.AreEqual(true, success);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EverCraft_Rogue_Cannont_Be_Good_Alignment()
        {
            rogue.Alignment = Alignment.Good;
        }

        //  MONK
        [TestMethod]
        public void EverCraft_Monk_Gains_Six_Hit_Points_Per_Level()
        {
            Assert.AreEqual(6, monk.HitPoints);
        }
        [TestMethod]
        public void EverCraft_Monk_Deals_Three_Points_Of_Damage()
        {
            monk.Attack(opponent, 10);

            Assert.AreEqual(2, opponent.HitPoints);
        }
        [TestMethod]
        public void EverCraft_Monk_Add_Wisdom_Bonus_To_Armor_Class()
        {
            monk.Dexterity = 14;
            monk.Wisdom = 14;

            Assert.AreEqual(14, monk.ArmorClass);
        }
        [TestMethod]
        public void EverCraft_Monk_Attack_Bonus_Increases_Every_Second_And_Third_Level()
        {
            monk.Experience = 4000;
            bool success = monk.Attack(opponent, 7);

            Assert.AreEqual(true, success);
        }

        //  PALADIN
        [TestMethod]
        public void EverCraft_Paladin_Gains_Eight_Hit_Points_Per_Level()
        {
            Assert.AreEqual(8, paladin.HitPoints);
        }
        [TestMethod]
        public void EverCraft_Paladin_Gains_Bonus_To_Attack_Rolls_Against_Evil_Characters()
        {
            opponent.Alignment = Alignment.Evil;
            bool success = paladin.Attack(opponent, 8);

            Assert.AreEqual(true, success);
        }
        [TestMethod]
        public void EverCraft_Paladin_Deals_Extra_Damage_To_Evil_Characters()
        {
            opponent.Alignment = Alignment.Evil;
            paladin.Attack(opponent, 10);

            Assert.AreEqual(2, opponent.HitPoints);
        }
        [TestMethod]
        public void EverCraft_Paladin_Deals_Triple_Damage_To_Evil_Characters_On_Critical_Hits()
        {
            opponent.Alignment = Alignment.Evil;
            paladin.Attack(opponent, 20);

            Assert.AreEqual(-4, opponent.HitPoints);
        }
        [TestMethod]
        public void EverCraft_Paladin_Attack_Bonuse_Increases_Every_Level()
        {
            paladin.Experience = 4000;
            bool success = paladin.Attack(opponent, 5);

            Assert.AreEqual(true, success);
        }
    }
}
