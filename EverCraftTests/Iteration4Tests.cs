﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EverCraft;
using EverCraft.Armor;
using EverCraft.Classes;
using EverCraft.Items;
using EverCraft.Weapons;

namespace EverCraftTests
{
    [TestClass]
    public class Iteration4Tests
    {
        private ECCharacter character;
        private ECCharacter opponent;

        private ECCharacter fighter;
        private ECCharacter monk;
        private ECCharacter paladin;

        private ECCharacter dwarf;
        private ECCharacter halfling;
        private ECCharacter elf;

        private ECWeapon longsword;
        private ECWeapon magicWaraxe;
        private ECWeapon elvenLongsword;
        private ECWeapon nunchucks;

        private ECArmor leatherArmor;
        private ECArmor plateArmor;
        private ECArmor magicLeatherArmor;
        private ECArmor elvenChailMail;

        private ECShield shield;

        private ECItem ringOfProtection;
        private ECItem beltOfGiantStrength;
        private ECItem amuletOfTheHeavens;

        [TestInitialize]
        public void Init()
        {
            character = new ECCharacter();
            opponent = new ECCharacter();

            fighter = new ECFighter();
            monk = new ECMonk();
            paladin = new ECPaladin();

            dwarf = new ECCharacter { Race = Race.Dwarf };
            halfling = new ECCharacter { Race = Race.Halfling };
            elf = new ECCharacter { Race = Race.Elf };

            longsword = new Longsword();
            magicWaraxe = new Waraxe(2);
            elvenLongsword = new ElvenLongsword();
            nunchucks = new Nunchucks();

            leatherArmor = new LeatherArmor();
            plateArmor = new PlateArmor();
            magicLeatherArmor = new LeatherArmorOfDamageReduction();
            elvenChailMail = new ElvenChainMail();

            shield = new Shield();

            ringOfProtection = new RingOfProtection();
            beltOfGiantStrength = new BeltOfGiantStrength();
            amuletOfTheHeavens = new AmuletOfTheHeavens();
        }

        #region weapons

        //  LONGSWORD
        [TestMethod]
        public void EverCraft_Longsword_Deals_Five_Damage()
        {
            Assert.AreEqual(5, longsword.DamageValue);
        }

        //  WARAXE +2
        [TestMethod]
        public void EverCraft_Waraxe_Deals_Six_Damage()
        {
            Assert.AreEqual(6, magicWaraxe.DamageValue);
        }
        [TestMethod]
        public void EverCraft_Magic_Waraxe_Grants_Attack_Bonus_Of_Two()
        {
            Assert.AreEqual(2, magicWaraxe.TotalAttack() - magicWaraxe.AttackValue);
        }
        [TestMethod]
        public void EverCraft_Magic_Waraxe_Grants_Damage_Bonus_Of_Two()
        {
            Assert.AreEqual(2, magicWaraxe.TotalDamage() - magicWaraxe.DamageValue);
        }
        [TestMethod]
        public void EverCraft_Waraxe_Deals_Triple_Damage_On_Critical_Hits()
        {
            character.Weapon = new Waraxe();
            bool success = character.Attack(opponent, 20);

            Assert.AreEqual(-13, opponent.HitPoints);
        }
        [TestMethod]
        public void EverCraft_Waraxe_Deals_Quadruple_Damage_On_Critical_Hits_For_Rogues()
        {
            character.Weapon = new Waraxe();
            bool success = character.Attack(opponent, 20);

            Assert.AreEqual(-13, opponent.HitPoints);
        }

        //  ELVEN LONGSWORD
        [TestMethod]
        public void EverCraft_Elven_Longsword_Deals_Five_Damage()
        {
            Assert.AreEqual(5, elvenLongsword.DamageValue);
        }
        [TestMethod]
        public void EverCraft_Elven_Longsword_Grants_Attack_Bonus_Of_One()
        {
            Assert.AreEqual(1, elvenLongsword.TotalAttack() - elvenLongsword.AttackValue);
        }
        [TestMethod]
        public void EverCraft_Elven_Longsword_Grans_Damage_Bonus_Of_One()
        {
            Assert.AreEqual(1, elvenLongsword.TotalDamage() - elvenLongsword.DamageValue);
        }
        [TestMethod]
        public void EverCraft_Elven_Longsword_Grants_Attack_Bonus_Of_Two_For_Elves()
        {
            character.Race = Race.Elf;

            Assert.AreEqual(2, elvenLongsword.TotalAttack(character, opponent) - elvenLongsword.AttackValue);
        }
        [TestMethod]
        public void EverCraft_Elven_Longsword_Grans_Damage_Bonus_Of_Two_For_Elves()
        {
            character.Race = Race.Elf;

            Assert.AreEqual(2, elvenLongsword.TotalDamage(character, opponent) - elvenLongsword.DamageValue);
        }
        [TestMethod]
        public void EverCraft_Elven_Longsword_Grants_Attack_Bonus_Of_Two_Against_Orcs()
        {
            opponent.Race = Race.Orc;

            Assert.AreEqual(2, elvenLongsword.TotalAttack(character, opponent) - elvenLongsword.AttackValue);
        }
        [TestMethod]
        public void EverCraft_Elven_Longsword_Grans_Damage_Bonus_Of_Two_Against_Orcs()
        {
            opponent.Race = Race.Orc;

            Assert.AreEqual(2, elvenLongsword.TotalDamage(character, opponent) - elvenLongsword.DamageValue);
        }
        [TestMethod]
        public void EverCraft_Elven_Longsword_Grants_Attack_Bonus_Of_Five_For_Elves_Against_Orcs()
        {
            character.Race = Race.Elf;
            opponent.Race = Race.Orc;

            Assert.AreEqual(5, elvenLongsword.TotalAttack(character, opponent) - elvenLongsword.AttackValue);
        }
        [TestMethod]
        public void EverCraft_Elven_Longsword_Grans_Damage_Bonus_Of_Five_For_Elves_Against_Orcs()
        {
            character.Race = Race.Elf;
            opponent.Race = Race.Orc;

            Assert.AreEqual(5, elvenLongsword.TotalDamage(character, opponent) - elvenLongsword.DamageValue);
        }

        // NUNCHUCKS
        [TestMethod]
        public void EverCraft_NunChucks_Deal_Six_Damage()
        {
            Assert.AreEqual(6, nunchucks.DamageValue);
        }
        [TestMethod]
        public void EverCraft_NunChucks_Grants_Attack_Penalty_Of_Four_For_Non_Monks()
        {
            Assert.AreEqual(-4, nunchucks.TotalAttack(fighter) - nunchucks.AttackValue);
        }

        #endregion

        #region armor & shields

        //  LEATHER ARMOR
        [TestMethod]
        public void EverCraft_Leather_Armor_Grants_Armor_Bonus_Of_Two()
        {
            Assert.AreEqual(2, leatherArmor.TotalArmor());
        }

        //  PLATE ARMOR
        [TestMethod]
        public void EverCraft_Plate_Armor_Grant_Armor_Bonus_Of_Eight()
        {
            Assert.AreEqual(8, plateArmor.TotalArmor());
        }
        [TestMethod]
        public void EverCraft_Plate_Armor_Can_Be_Equipped_By_Fighters()
        {
            fighter.Armor = plateArmor;

            Assert.AreEqual(plateArmor, fighter.Armor);
        }
        [TestMethod]
        public void EverCraft_Plate_Armor_Cannot_Be_Equipped_By_Non_Fighters()
        {
            monk.Armor = plateArmor;

            Assert.AreEqual(null, monk.Armor);
        }
        [TestMethod]
        public void EverCraft_Plate_Armor_Can_Be_Equipped_By_Dwarves()
        {
            dwarf.Armor = plateArmor;

            Assert.AreEqual(plateArmor, dwarf.Armor);
        }
        [TestMethod]
        public void EverCraft_Plate_Armor_Cannot_Be_Equipped_By_Non_Dwarves()
        {
            halfling.Armor = plateArmor;

            Assert.AreEqual(null, halfling.Armor);
        }

        //  LEATHER ARMOR OF DAMAGE REDUCTION
        [TestMethod]
        public void EverCraft_Magic_Leather_Armor_Grants_Armor_Bonus_Of_Two()
        {
            Assert.AreEqual(2, magicLeatherArmor.TotalArmor());
        }
        [TestMethod]
        public void EverCraft_Magic_Leater_Armor_Grants_Damage_Reduction_Of_Two()
        {
            Assert.AreEqual(2, magicLeatherArmor.DamageReduction);
        }

        //  ELVEN CHAIN MAIL
        [TestMethod]
        public void EverCraft_Elven_Chain_Mail_Grants_Armor_Bonus_Of_Five()
        {
            Assert.AreEqual(5, elvenChailMail.TotalArmor());
        }
        [TestMethod]
        public void EverCraft_Elven_Chain_Mail_Grants_Armor_Bonus_Of_Eight_For_Elves()
        {
            Assert.AreEqual(8, elvenChailMail.TotalArmor(elf));
        }
        [TestMethod]
        public void EverCraft_Elven_Chain_Mail_Grants_Attack_Bonus_Of_One_For_Elves()
        {
            Assert.AreEqual(1, elvenChailMail.TotalAttack(elf));
        }

        //  SHIELD
        [TestMethod] 
        public void EverCraft_Shield_Grants_Armor_Bonus_Of_Three()
        {
            Assert.AreEqual(3, shield.TotalArmor());
        }
        [TestMethod]
        public void EverCraft_Shield_Grants_Attack_Penalty_Of_Four()
        {
            Assert.AreEqual(-4, shield.TotalAttack());
        }
        [TestMethod]
        public void EverCraft_Shield_Grants_Attack_Penalty_Of_Two_For_Fighters()
        {
            Assert.AreEqual(-2, shield.TotalAttack(fighter));
        }

        #endregion

        #region items

        [TestMethod]
        public void EverCraft_Ring_Of_Protection_Grants_Armor_Bonus_Of_Two()
        {
            Assert.AreEqual(2, ringOfProtection.ArmorBonus);
        }

        [TestMethod]
        public void EverCraft_Belt_Of_Giant_Strength_Grants_Strength_Bonus_Of_Four()
        {
            Assert.AreEqual(4, beltOfGiantStrength.StrengthBonus);
        }

        [TestMethod]
        public void EverCraft_Amulet_Of_The_Heavens_Grants_Attack_Bonus_Of_One_Against_Neutral_Enemies()
        {
            Assert.AreEqual(1, amuletOfTheHeavens.TotalAttack(character, opponent));
        }
        [TestMethod]
        public void EverCraft_Amulet_Of_The_Heavens_Grants_Attack_Bonus_Of_Two_Against_Evil_Enemies()
        {
            opponent.Alignment = Alignment.Evil;

            Assert.AreEqual(2, amuletOfTheHeavens.TotalAttack(character, opponent));
        }
        [TestMethod]
        public void EverCraft_Amulet_Of_The_Heavens_Grants_Attack_Bonus_Of_Two_For_Paladins_Against_Neutral_Enemies()
        {
            Assert.AreEqual(2, amuletOfTheHeavens.TotalAttack(paladin, opponent));
        }
        [TestMethod]
        public void EverCraft_Amulet_Of_The_Heavens_Grants_Attack_Bonus_Of_Four_For_Paladins_Against_Evil_Enemies()
        {
            opponent.Alignment = Alignment.Evil;

            Assert.AreEqual(4, amuletOfTheHeavens.TotalAttack(paladin, opponent));
        }

        #endregion
    }
}
