﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Items
{
    public class RingOfProtection : ECItem
    {
        public override int ArmorBonus => 2;
    }
}
