﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Items
{
    public class BeltOfGiantStrength : ECItem
    {
        public override int StrengthBonus => 4;
    }
}
