﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Items
{
    public class ECItems
    {
        private IList<ECItem> _items;

        public int ArmorBonus => _items.Sum(x => x.ArmorBonus);

        public int StrengthBonus => _items.Sum(x => x.StrengthBonus);
        public int DeterityBonus => _items.Sum(x => x.DexterityBonus);
        public int ConstitutionBonus => _items.Sum(x => x.ConstitutionBonus);
        public int IntelligenceBonus => _items.Sum(x => x.IntelligenceBonus);
        public int WisdomBonus => _items.Sum(x => x.WisdomBonus);
        public int CharismaBonus => _items.Sum(x => x.CharismaBonus);

        public int StrengthModifierBonus => _items.Sum(x => x.StrengthModifierBonus);
        public int DeterityModifierBonus => _items.Sum(x => x.DexterityModifierBonus);
        public int ConstitutionModifierBonus => _items.Sum(x => x.ConstitutionModifierBonus);
        public int IntelligenceModifierBonus => _items.Sum(x => x.IntelligenceModifierBonus);
        public int WisdomModifierBonus => _items.Sum(x => x.WisdomModifierBonus);
        public int CharismaModifierBonus => _items.Sum(x => x.CharismaModifierBonus);

        public IList<ECItem> ItemList
        {
            get { return _items; }
            set { _items = value; }
        }

        public ECItems()
        {
            _items = new List<ECItem>();
        }

        public int TotalAttack(ECCharacter wearer = null, ECCharacter opponent = null)
        {
            return _items.Sum(x => x.TotalAttack(wearer, opponent));
        }
    }
}
