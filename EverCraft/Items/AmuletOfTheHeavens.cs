﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Items
{
    public class AmuletOfTheHeavens : ECItem
    {
        public override int TotalAttack(ECCharacter wearer = null, ECCharacter opponent = null)
        {
            int totalAttack = opponent.Alignment == Alignment.Neutral ? 1
                : opponent.Alignment == Alignment.Evil ? 2 : 0;
            if (wearer.Class == PlayerClass.Paladin) { totalAttack *= 2; }
            return totalAttack;
        }
    }
}
