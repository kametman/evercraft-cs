﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Items
{
    public abstract class ECItem : ECEquipment
    {
        public override string Name => "Item";
        public override string Description => "???";

        public virtual int ArmorBonus => 0;

        public virtual int StrengthBonus => 0;
        public virtual int DexterityBonus => 0;
        public virtual int ConstitutionBonus => 0;
        public virtual int IntelligenceBonus => 0;
        public virtual int WisdomBonus => 0;
        public virtual int CharismaBonus => 0;

        public virtual int StrengthModifierBonus => 0;
        public virtual int DexterityModifierBonus => 0;
        public virtual int ConstitutionModifierBonus => 0;
        public virtual int IntelligenceModifierBonus => 0;
        public virtual int WisdomModifierBonus => 0;
        public virtual int CharismaModifierBonus => 0;

        public virtual int TotalAttack(ECCharacter wearer = null, ECCharacter opponent = null)
        {
            return 0;
        }
    }
}
