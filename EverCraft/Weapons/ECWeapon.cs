﻿using System;

namespace EverCraft.Weapons
{
    public enum WeaponType
    {
        Longsword,
        Waraxe,
        Nunchucks,
    }

    public abstract class ECWeapon : ECEquipment
    {
        protected int _bonus;

        public override string Name => "Weapon";
        public override string Description => $"+{DamageValue} DMG";

        public abstract WeaponType WeaponType { get; }
        public int Bonus => _bonus;
        public virtual int DamageValue => 1;
        public virtual int AttackValue => 0;
        public virtual int CriticalMultipler => 2;

        public ECWeapon(int bonus = 0)
        {
            _bonus = bonus;
        }

        public virtual int TotalDamage(ECCharacter wielder = null, ECCharacter opponent = null)
        {
            return DamageValue + Bonus;
        }
        public virtual int TotalAttack(ECCharacter wielder = null, ECCharacter opponent = null)
        {
            return AttackValue + _bonus;
        }
    }
}
