﻿using System;

namespace EverCraft.Weapons
{
    public class Longsword : ECWeapon
    {
        public override WeaponType WeaponType => WeaponType.Longsword;
        public override int DamageValue => 5;

        public Longsword(int bonus = 0) : base(bonus) { }
    }
}
