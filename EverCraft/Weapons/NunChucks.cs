﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Weapons
{
    public class Nunchucks : ECWeapon
    {
        public override WeaponType WeaponType => WeaponType.Nunchucks;
        public override int DamageValue => 6;

        public Nunchucks(int bonus = 0) : base(bonus) { }

        public override int TotalAttack(ECCharacter wielder = null, ECCharacter opponent = null)
        {
            int totalAttack = base.TotalAttack(wielder, opponent);
            if (wielder != null && wielder.Class != PlayerClass.Monk) { totalAttack -= 4; }
            return totalAttack;
        }
    }
}
