﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Weapons
{
    public class ElvenLongsword : Longsword
    {
        public ElvenLongsword() : base(1) { }

        public override int TotalDamage(ECCharacter wielder = null, ECCharacter opponent = null)
        {
            int totalDamage = base.TotalDamage(wielder, opponent);
            if (wielder != null && opponent != null)
            {
                if (wielder.Race == Race.Elf && opponent.Race == Race.Orc) { totalDamage += 4; }
                else if (wielder.Race == Race.Elf || opponent.Race == Race.Orc) { totalDamage += 1; }
            }
            return totalDamage;
        }

        public override int TotalAttack(ECCharacter wielder = null, ECCharacter opponent = null)
        {
            int totalAttack = base.TotalAttack(wielder, opponent);
            if (wielder != null && opponent != null)
            {
                if (wielder.Race == Race.Elf && opponent.Race == Race.Orc) { totalAttack += 4; }
                else if (wielder.Race == Race.Elf || opponent.Race == Race.Orc) { totalAttack += 1; }
            }
            return totalAttack;
        }
    }
}
