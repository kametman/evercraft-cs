﻿using System;

namespace EverCraft.Weapons
{
    public class Waraxe : ECWeapon
    {
        public override WeaponType WeaponType => WeaponType.Waraxe;
        public override int DamageValue => 6;
        public override int CriticalMultipler => 3;

        public Waraxe(int bonus = 0) : base(bonus) { }
    }
}
