﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Armor
{
    public class ChainMail : ECArmor
    {
        public override string Name => "Chain Mail";

        public override int ArmorClass => 5;
        public ChainMail(int bonus = 0) : base(bonus) { }
    }
}
