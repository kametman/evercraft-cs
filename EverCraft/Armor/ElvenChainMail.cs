﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Armor
{
    public class ElvenChainMail : ChainMail
    {
        public override string Name => "Elven Chain Mail";
        public ElvenChainMail(int bonus = 0) : base(bonus) { }

        public override int TotalArmor(ECCharacter wearer = null)
        {
            int totalArmor = base.TotalArmor();
            if (wearer != null)
            {
                if (wearer.Race == Race.Elf) { totalArmor += 3; }
            }
            return totalArmor;
        }

        public override int TotalAttack(ECCharacter wearer = null)
        {
            int totalAttack = base.TotalAttack(wearer);
            if (wearer != null)
            {
                if (wearer.Race == Race.Elf) { totalAttack += 1; }
            }
            return totalAttack;
        }
    }
}
