﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Armor
{
    public class Shield : ECShield
    {
        public override string Name => "Shield";
        public override string Description => "Generic shield.";

        public override int ArmorClass => 3;
        public override int AttackBonus => -4;

        public Shield(int bonus = 0) : base(bonus) { }

        public override int TotalAttack(ECCharacter wearer = null)
        {
            int totalAttack = base.TotalAttack(wearer);
            if (wearer != null)
            {
                if (wearer.Class == PlayerClass.Fighter) { totalAttack += 2; }
            }
            return totalAttack;
        }
    }
}
