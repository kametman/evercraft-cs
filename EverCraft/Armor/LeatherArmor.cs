﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Armor
{
    public class LeatherArmor : ECArmor
    {
        public override string Name => "Leather Armor";

        public override int ArmorClass => 2;

        public LeatherArmor(int bonus = 0) : base(bonus) { }
    }
}
