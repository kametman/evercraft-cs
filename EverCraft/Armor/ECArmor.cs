﻿using System;

namespace EverCraft.Armor
{
    public abstract class ECArmor : ECEquipment
    {
        protected int _bonus;

        public override string Name => "Armor";
        public override string Description => $"+{ArmorClass} AC";

        public virtual int ArmorClass => 0;
        public virtual int DamageReduction => 0;
        public virtual int AttackBonus => 0;

        public ECArmor(int bonus = 0)
        {
            _bonus = bonus;
        }

        public virtual int TotalArmor(ECCharacter wearer = null)
        {
            return ArmorClass + _bonus;
        }
        public virtual int TotalAttack(ECCharacter wearer = null)
        {
            return AttackBonus + _bonus;
        }

        public virtual bool CanEquip(ECCharacter wearer)
        {
            return true;
        }
    }
}
