﻿using System;

namespace EverCraft.Armor
{
    public class ECShield : ECEquipment
    {
        protected int _bonus;

        public override string Name => string.Empty;
        public override string Description => string.Empty;

        public virtual int ArmorClass => 0;
        public virtual int AttackBonus => 0;

        public ECShield(int bonus = 0)
        {
            _bonus = bonus;
        }


        public virtual int TotalArmor(ECCharacter wearer = null)
        {
            return ArmorClass + _bonus;
        }
        public virtual int TotalAttack(ECCharacter wearer = null)
        {
            return AttackBonus + _bonus;
        }
    }
}
