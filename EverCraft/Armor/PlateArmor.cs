﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Armor
{
    public class PlateArmor : ECArmor
    {
        public override string Name => "Plate Armor";

        public override int ArmorClass => 8;

        public PlateArmor(int bonus = 0) : base(bonus) { }

        public override bool CanEquip(ECCharacter wearer)
        {
            return wearer.Class == PlayerClass.Fighter || wearer.Race == Race.Dwarf;
        }
    }
}
