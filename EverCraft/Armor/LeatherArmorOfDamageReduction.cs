﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft.Armor
{
    public class LeatherArmorOfDamageReduction : LeatherArmor
    {
        public override int DamageReduction => 2;
        public LeatherArmorOfDamageReduction(int bonus = 0) : base(bonus) { }
    }
}
