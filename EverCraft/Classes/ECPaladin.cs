﻿using System;

namespace EverCraft.Classes
{
    public class ECPaladin : ECCharacter
    {
        public new Alignment Alignment
        {
            get { return _alignment; }
            set
            {
                if (value != Alignment.Good)
                {
                    throw new ArgumentException("Paladin must be Good alignment.");
                }
                _alignment = value;
            }
        }
        public override PlayerClass Class => PlayerClass.Paladin;

        public ECPaladin() : base()
        {
            _baseHitPoints = 8;
            LevelUp();
        }

        public new bool Attack(ECCharacter opponent, int attackRoll)
        {
            bool attackingEvil = opponent.Alignment == Alignment.Evil;
            int attackBonus = StrengthModifier + Level + RacialAttackBonus(opponent);
            attackBonus += GetAdditionalAttackBonuses(opponent);
            if (attackingEvil) { attackBonus += 2; }
            bool success = opponent.IsHit(attackRoll, attackBonus);
            if (success)
            {
                int damage = GetBaseDamage(opponent) + StrengthModifier + RacialDamageBonus(opponent);
                if (attackingEvil) { damage += 2; }
                if (IsCriticalHit(attackRoll)) { damage *= GetCriticalMultipler() + (attackingEvil ? 1 : 0); }
                if (damage < 1) { damage = 1; }
                opponent.TakeDamage(damage);
                Experience += 10;
            }
            return success;
        }
    }
}
