﻿using System;

namespace EverCraft.Classes
{
    public class ECRogue : ECCharacter
    {
        public new Alignment Alignment
        {
            get { return _alignment; }
            set
            {
                if (value == Alignment.Good)
                {
                    throw new ArgumentException("Rogue cannot be Good alignment.");
                }
                _alignment = value;
            }
        }
        public override PlayerClass Class => PlayerClass.Rogue;
        public ECRogue() : base() { }

        public new bool Attack(ECCharacter opponent, int attackRoll)
        {
            int attackBonus = DexterityModifier + Level / 2 + RacialAttackBonus(opponent);
            attackBonus += GetAdditionalAttackBonuses(opponent);
            bool success = opponent.IsHit(attackRoll, attackBonus, true);
            if (success)
            {
                int damage = GetBaseDamage(opponent) + StrengthModifier + RacialDamageBonus(opponent);
                if (IsCriticalHit(attackRoll)) { damage *= GetCriticalMultipler() + 1; }
                if (damage < 1) { damage = 1; }
                opponent.TakeDamage(damage);
                Experience += 10;
            }
            return success;
        }
    }
}
