﻿namespace EverCraft.Classes
{
    public class ECFighter : ECCharacter
    {
        public override PlayerClass Class => PlayerClass.Fighter;

        public ECFighter() : base()
        {
            _baseHitPoints = 10;
            LevelUp();
        }

        public new bool Attack(ECCharacter opponent, int attackRoll)
        {
            int attackBonus = StrengthModifier + Level + RacialAttackBonus(opponent);
            attackBonus += GetAdditionalAttackBonuses(opponent);
            bool success = opponent.IsHit(attackRoll, attackBonus);
            if (success)
            {
                int damage = GetBaseDamage(opponent) + StrengthModifier + RacialDamageBonus(opponent);
                if (IsCriticalHit(attackRoll)) { damage *= GetCriticalMultipler(); }
                if (damage < 1) { damage = 1; }
                opponent.TakeDamage(damage);
                Experience += 10;
            }
            return success;
        }
    }
}
