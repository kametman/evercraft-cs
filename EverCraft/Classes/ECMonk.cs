﻿namespace EverCraft.Classes
{
    public class ECMonk : ECCharacter
    {
        public override PlayerClass Class => PlayerClass.Monk;

        public new int ArmorClass
        {
            get
            {
                int wisBonus = WisdomModifier < 0 ? 0 : WisdomModifier;
                return base.ArmorClass + WisdomModifier;
            }
        }
        public ECMonk() : base()
        {
            _baseHitPoints = 6;
            _baseDamage = 3;
            LevelUp();
        }

        public new bool Attack(ECCharacter opponent, int attackRoll)
        {
            int attackBonus = StrengthModifier + Level / 2 + Level / 3 + RacialAttackBonus(opponent);
            attackBonus += GetAdditionalAttackBonuses(opponent);
            bool success = opponent.IsHit(attackRoll, attackBonus);
            if (success)
            {
                int damage = GetBaseDamage(opponent) + StrengthModifier + RacialDamageBonus(opponent);
                if (IsCriticalHit(attackRoll)) { damage *= GetCriticalMultipler(); }
                if (damage < 1) { damage = 1; }
                opponent.TakeDamage(damage);
                Experience += 10;
            }
            return success;
        }
    }
}
