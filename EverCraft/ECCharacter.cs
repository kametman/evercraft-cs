﻿using System;
using EverCraft.Armor;
using EverCraft.Items;
using EverCraft.Weapons;

namespace EverCraft
{
    public enum Alignment { Good, Evil, Neutral }
    public enum PlayerClass
    {
        None,
        Fighter,
        Rogue,
        Monk,
        Paladin,
    }
    public enum Race {
        Human,
        Orc,
        Dwarf,
        Elf,
        Halfling
    }

    public class ECCharacter
    {
        public string Name { get; set; }

        protected Alignment _alignment;
        public Alignment Alignment
        {
            get { return _alignment; }
            set
            {
                if (Race == Race.Halfling && value == Alignment.Evil)
                {
                    throw new ArgumentException("Halflings cannot be Evil alignment.");
                }
                _alignment = value;
            }
        }

        public virtual PlayerClass Class => PlayerClass.None;

        protected Race _race;
        public Race Race
        {
            get { return _race; }
            set
            {
                if (value == Race.Halfling && Alignment == Alignment.Evil)
                {
                    throw new ArgumentException("Halflings cannot be Evil alignment.");
                }
                _race = value;
                LevelUp();
            }
        }

        public int BaseArmorClass { get; set; }
        public int ArmorClass
        {
            get
            {
                int totalArmor = BaseArmorClass + DexterityModifier + RacialArmorClassModifier();
                if (Armor != null) { totalArmor += Armor.TotalArmor(this); }
                totalArmor += Items.ArmorBonus;
                return totalArmor;
            }
        }

        protected int _baseHitPoints;
        protected int _maximumHitPoints;
        protected int _currentHitPoints;
        public int HitPoints
        {
            get { return _currentHitPoints; }
            set
            {
                _currentHitPoints = value > _maximumHitPoints * Level
                    ? _maximumHitPoints * Level
                    : value;
            }
        }
        public bool IsAlive { get { return HitPoints > 0; } }

        protected int _experience;
        public int Experience
        {
            get { return _experience; }
            set
            {
                int currentLevel = Level;
                _experience = value;
                if (Level > currentLevel) { LevelUp(); }
            }
        }
        public int Level { get { return 1 + Experience / 1000; } }

        protected int _strength;
        public int Strength
        {
            get { return _strength + Items.StrengthBonus; }
            set { _strength = value; }
        }
        public int StrengthModifier
        {
            get { return Strength / 2 - 5 + RacialStrengthModifier() + Items.StrengthModifierBonus; }
        }

        protected int _dexterity;
        public int Dexterity
        {
            get { return _dexterity + Items.DeterityBonus; }
            set { _dexterity = value; }
        }
         public int DexterityModifier
        {
            get { return Dexterity / 2 - 5 + RacialDexterityModifier() + Items.DeterityModifierBonus; }
        }

        protected int _constitution;
        public int Constitution
        {
            get { return _constitution + Items.ConstitutionBonus; }
            set
            {
                _constitution = value;
                LevelUp();
            }
        }
        public int ConstitutionModifier
        {
            get { return Constitution / 2 - 5 + RacialConstitutionModifier() + Items.ConstitutionModifierBonus; }
        }

        protected int _intelligence;
        public int Intelligence
        {
            get { return _intelligence + Items.IntelligenceBonus; }
            set { _intelligence = value; }
        }
        public int IntelligenceModifier
        {
            get { return Intelligence / 2 - 5 + RacialIntelligenceModifier() + Items.IntelligenceModifierBonus; }
        }

        protected int _wisdom;
        public int Wisdom
        {
            get { return _wisdom + Items.WisdomBonus; }
            set { _wisdom = value; }
        }
        public int WisdomModifier
        {
            get { return Wisdom / 2 - 5 + RacialWisdomModifier() + Items.WisdomModifierBonus; }
        }

        protected int _charisma;
        public int Charisma
        {
            get { return _charisma + Items.CharismaBonus; }
            set { _charisma = value; }
        }
        public int CharismaModifier
        {
            get { return Charisma / 2 - 5 + RacialCharismaModifier() + Items.CharismaModifierBonus; }
        }

        protected int _baseDamage;
        protected int _criticalHitRange;

        public ECWeapon Weapon { get; set; }

        protected ECArmor _armor;
        public ECArmor Armor
        {
            get { return _armor; }
            set
            {
                if (value.CanEquip(this)) { _armor = value; }
            }
        }

        public ECShield Shield { get; set; }

        public ECItems Items { get; set; }

        public ECCharacter()
        {
            Items = new ECItems();

            Race = Race.Human;
            Alignment = Alignment.Neutral;

            Strength = 10;
            Dexterity = 10;
            Constitution = 10;
            Intelligence = 10;
            Wisdom = 10;
            Charisma = 10;

            BaseArmorClass = 10;

            _baseHitPoints = 5;
            _maximumHitPoints = _baseHitPoints + ConstitutionModifier;
            if (_maximumHitPoints < 0) { _maximumHitPoints = 1; }
            _currentHitPoints = _maximumHitPoints * Level;

            _baseDamage = 1;
            _criticalHitRange = 1;
        }

        public bool Attack(ECCharacter opponent, int attackRoll)
        {
            int attackBonus = StrengthModifier + Level / 2 + RacialAttackBonus(opponent);
            attackBonus += GetAdditionalAttackBonuses(opponent);
            bool success = opponent.IsHit(attackRoll, attackBonus);
            if (success)
            {
                int damage = GetBaseDamage(opponent) + StrengthModifier + RacialDamageBonus(opponent);
                if (IsCriticalHit(attackRoll)) { damage *= GetCriticalMultipler(); }
                if (damage < 1) { damage = 1; }
                opponent.TakeDamage(damage);
                Experience += 10;
            }
            return success;
        }
        protected int GetAdditionalAttackBonuses(ECCharacter opponent)
        {
            int attackBonus = 0;
            if (Weapon != null) { attackBonus += Weapon.TotalAttack(this, opponent); }
            if (Armor != null) { attackBonus += Armor.TotalAttack(this); }
            if (Shield != null) { attackBonus += Shield.TotalAttack(this); }
            attackBonus += Items.TotalAttack(this, opponent);
            return attackBonus;
        }

        public bool IsHit(int attackRoll, int attackBonus, bool ignoreDexerityBonus = false)
        {
            int totalAttack = attackRoll + attackBonus;
            if (ignoreDexerityBonus)
            {
                int dexBonus = DexterityModifier < 0 ? 0 : DexterityModifier;
                return totalAttack >= ArmorClass - dexBonus || attackRoll == 20;
            }
            else { return totalAttack >= ArmorClass || IsCriticalHit(attackRoll); }
            
        }
        public bool IsCriticalHit(int attackRoll)
        {
            var critRange = _criticalHitRange;
            if (Race == Race.Elf) { critRange++; }
            return attackRoll > (20 - critRange) && attackRoll <= 20;
        }
        public bool TakeDamage(int damage)
        {
            if (Armor != null) { damage -= Armor.DamageReduction; }
            HitPoints -= damage;
            return IsAlive;
        }
        public int GetBaseDamage(ECCharacter opponent)
        {
            return Weapon == null ? _baseDamage : Weapon.TotalDamage(this, opponent);
        }
        public int GetCriticalMultipler()
        {
            return Weapon == null ? 2 : Weapon.CriticalMultipler;
        }

        protected void LevelUp()
        {
            _maximumHitPoints = (_baseHitPoints + ConstitutionModifier) * Level;
            if (_maximumHitPoints < 0) { _maximumHitPoints = 1; }
            if (Race == Race.Dwarf && ConstitutionModifier > 0) { _maximumHitPoints += ConstitutionModifier * Level; }
            _currentHitPoints = _maximumHitPoints;
        }

        protected int RacialArmorClassModifier()
        {
            switch (Race)
            {
                case Race.Orc: return 2;
                default: return 0;
            }
        }
        protected int RacialAttackBonus(ECCharacter defender)
        {
            int attackBonus = 0;
            if (Race == Race.Dwarf && defender.Race == Race.Orc) { attackBonus += 2; }
            if (Race == Race.Orc && defender.Race == Race.Elf) { attackBonus -= 2; }
            if (Race != Race.Halfling && defender.Race == Race.Halfling) { attackBonus -= 2; }
            return attackBonus;
        }
        protected int RacialDamageBonus(ECCharacter defender)
        {
            switch (Race)
            {
                case Race.Dwarf: return defender.Race == Race.Orc ? 2 : 0;
                default: return 0;
            }
        }

        protected int RacialStrengthModifier()
        {
            switch (Race)
            {
                case Race.Orc: return 2;
                case Race.Halfling: return -1;
                default: return 0;
            }
        }
        protected int RacialDexterityModifier()
        {
            switch (Race)
            {
                case Race.Elf: return 1;
                case Race.Halfling: return 1;
                default: return 0;
            }
        }
        protected int RacialConstitutionModifier()
        {
            switch (Race)
            {
                case Race.Dwarf: return 1;
                case Race.Elf: return -1;
                default: return 0;
            }
        }
        protected int RacialIntelligenceModifier()
        {
            switch (Race)
            {
                case Race.Orc: return -1;
                default: return 0;
            }
        }
        protected int RacialWisdomModifier()
        {
            switch (Race)
            {
                case Race.Orc: return -1;
                default: return 0;
            }
        }
        protected int RacialCharismaModifier()
        {
            switch (Race)
            {
                case Race.Orc: return -1;
                case Race.Dwarf: return -1;
                default: return 0;
            }
        }
    }
}
