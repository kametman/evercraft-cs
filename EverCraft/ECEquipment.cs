﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EverCraft
{
    public abstract class ECEquipment
    {

        public abstract string Name { get; }
        public abstract string Description { get; }
    }
}
